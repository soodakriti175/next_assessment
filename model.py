#!/usr/bin/env python
# coding: utf-8

# In[1]:


# import packages
import numpy as np 
import pandas as pd 
import tensorflow as tf
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.preprocessing import OneHotEncoder
from sklearn.model_selection import train_test_split


# In[2]:


# importing training an testing data
train = pd.read_csv("train_data_evaluation_part_2.csv", index_col = 0)
test = pd.read_csv("test_data_evaluation_part2.csv", index_col = 0)


# In[3]:


train.head()


# In[4]:


# data exploration
def explore(data):
    print(data.shape)
    print(data.info())
    print(data.describe())
    print(data.columns[data.isnull().any()])
    print(data.ID.nunique()/data.shape[0])
    return 


# In[5]:


explore(train)


# In[6]:


test.head()


# In[7]:


explore(test)


# In[8]:


# Replacing null values using mean
def impute(data):
    data['Age'].fillna(data['Age'].mean(),inplace=True)
    return print(data.columns[data.isnull().any()])


# In[9]:


impute(train)
impute(test)


# In[10]:


plt.figure(figsize=(5, 5))
train["Age"].plot.box(vert=False)


# In[11]:


q_value = [i/100 for i in range (95,101,1)]
q1 = train['Age'].quantile(q_value)
q1


# In[12]:


train = train[(train["Age"]>0) & (train["Age"]<=80)]
plt.figure(figsize=(5, 5))
train["Age"].plot.box(vert=False)


# In[13]:


g = sns.catplot(x='DistributionChannel',y='BookingsCheckedIn',kind="bar",palette="viridis",data=train)
g.set_xticklabels(rotation=30)


# In[14]:


g = sns.catplot(x='MarketSegment',y='BookingsCheckedIn',kind="bar",palette="viridis",data=train)
g.set_xticklabels(rotation=30)


# In[15]:


Max_Checked_In = train[['BookingsCheckedIn','Nationality']].max()
Max_Checked_In


# In[16]:


df = pd.melt(train, id_vars=['ID', 'Nationality', 'Age', 'DaysSinceCreation', 'AverageLeadTime','LodgingRevenue', 'OtherRevenue', 'BookingsCanceled','BookingsNoShowed', 'BookingsCheckedIn', 'PersonsNights', 'RoomNights','DaysSinceLastStay', 'DaysSinceFirstStay', 'DistributionChannel','MarketSegment'], var_name = 'SR')
df['Total_Bookings'] = df['BookingsCheckedIn']-(df['BookingsCanceled']+df['BookingsNoShowed'])
df['Total_Days'] = df['DaysSinceLastStay']-df['DaysSinceFirstStay']


# In[17]:


plt.figure(figsize=(7,7))
df["SR"].value_counts().plot(kind="pie",autopct='%1.2f%%')


# In[18]:


g = sns.catplot(x='DistributionChannel',y='DaysSinceLastStay',kind="boxen",palette="Spectral",data=train)
g.set_xticklabels(rotation=30)


# In[19]:


g = sns.catplot(x='DistributionChannel',y='DaysSinceFirstStay',kind="violin",palette="Spectral",data=train)
g.set_xticklabels(rotation=30)


# In[20]:


g = sns.catplot(x='DistributionChannel',y='BookingsCanceled',palette="Spectral",data=train)
g.set_xticklabels(rotation=30)


# In[21]:


g = sns.catplot(x='DistributionChannel',y='BookingsNoShowed',data=train)
g.set_xticklabels(rotation=30)


# In[22]:


g = sns.catplot(x='MarketSegment',y='DaysSinceLastStay',kind="point",palette="Spectral",data=train)
g.set_xticklabels(rotation=60)


# In[23]:


g = sns.catplot(x='MarketSegment',y='DaysSinceFirstStay',kind="violin",palette="Spectral",data=train)
g.set_xticklabels(rotation=60)


# In[24]:


g = sns.catplot(x='MarketSegment',y='BookingsCanceled',palette="Spectral",data=train)
g.set_xticklabels(rotation=60)


# In[25]:


g = sns.catplot(x='MarketSegment',y='BookingsCanceled',palette="Spectral",data=train)
g.set_xticklabels(rotation=60)


# In[26]:


# Dropping ID and Nationality
train.drop(["ID","Nationality"], inplace = True, axis = 1 )
test.drop(["ID","Nationality"], inplace = True, axis = 1 )


# In[27]:


# Encoding categorical data to numerical data
train = pd.get_dummies(train, columns = ['DistributionChannel','MarketSegment'])
test = pd.get_dummies(test, columns = ['DistributionChannel','MarketSegment'])


# In[28]:


test['MarketSegment_Groups'] = 0


# In[29]:


# train = tf.keras.utils.normalize(train)


# In[30]:


X = train.drop("BookingsCheckedIn", axis = 1)
y = train["BookingsCheckedIn"]
X_test = test.drop("BookingsCheckedIn", axis = 1)
y_test = test["BookingsCheckedIn"]


# In[31]:


train_x , valid_x , train_y , valid_y = train_test_split(X,y, test_size = 0.05)


# In[32]:


train_x.shape


# In[33]:


X_test = test.drop("BookingsCheckedIn", axis = 1)
y_test = test["BookingsCheckedIn"]


# In[34]:


# importing packages 
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Flatten,BatchNormalization, MaxPooling1D,Conv1D, Dropout
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping
from tensorflow.keras import regularizers


# In[35]:


train_x = train_x.to_numpy()
train_x = train_x[...,np.newaxis]


# In[36]:


from keras.regularizers import l2
def get_conv_model(dropout_rate, wd):
    conv_model=Sequential([
        Conv1D(256, kernel_size=3, kernel_regularizer=l2(1e-4), kernel_initializer='he_normal',activation='relu',  input_shape = (train_x[0].shape),name="Conv_Layer_1"),
        Conv1D(256, kernel_size=3, activation='relu', name="Conv_Layer_2"),
        Conv1D(128, kernel_size=3, activation='relu', name="Conv_Layer_3"),
        Conv1D(128, kernel_size=3, activation='relu', name="Conv_Layer_4"),
        Conv1D(32, kernel_size=3, activation='relu', name="Conv_Layer_5"),
        Conv1D(32, kernel_size=3, activation='relu', name="Conv_Layer_6"),
        Dense(16, activation = 'relu', name="Dense_Layer_1"),
        Dense(16, activation = 'relu', name="Dense_Layer_2"),
        Dense(8, activation = 'relu', name="Dense_Layer_3"),
        Dense(8, activation = 'relu', name="Dense_Layer_4"),
        Dense(1, activation = 'sigmoid', name="Output_Layer")
    ])
    
    conv_model.compile(optimizer = 'Nadam',
                 loss = 'binary_crossentropy',
                 metrics = ['accuracy'])
    
    return conv_model


# In[37]:


conv_model = get_conv_model(0.25, 0.1)
conv_model.summary()


# In[66]:


def get_checkpoint_best_only_conv():
    
    checkpoint_path_best_only_conv = 'Desktop/Flask/venv/checkpoints_best_only/checkpoint'
    checkpoints_best_only_conv = ModelCheckpoint(filepath=checkpoint_path_best_only_conv,
                                           save_weights_only=True,
                                           monitor='val_accuracy',
                                           frequency="epoch",
                                           save_best_only=True,
                                            verbose=1)
    return checkpoints_best_only_conv



def get_early_stopping_conv():

    early_stopping_conv = tf.keras.callbacks.EarlyStopping(monitor='val_accuracy', 
                                                      patience=5,
                                                     verbose=1)
    return early_stopping_conv


# In[67]:


checkpoint_best_only_conv = get_checkpoint_best_only_conv()
early_stopping_conv = get_early_stopping_conv()
callbacks_conv = [checkpoint_best_only_conv, early_stopping_conv]


# In[68]:


def train_model(conv_model, train_data, train_targets, batch_size, epochs):
    
    history_conv = conv_model.fit(train_data, 
                        train_targets,
                        batch_size=batch_size,
                        epochs = epochs,
                        validation_data = (valid_x,valid_y),
                        callbacks=callbacks_conv)
    return history_conv
history_conv = train_model(conv_model, train_x, train_y,2000, 20)


# In[70]:


X_test = X_test.to_numpy()
X_test = X_test[...,np.newaxis]


# In[74]:


def get_test_accuracy_conv(conv_model, valid_x, valid_y):
    test_loss, test_acc = conv_model.evaluate(x=valid_x, y=valid_y, verbose=1)
    print('accuracy: {acc:0.3f}'.format(acc=test_acc))
    
get_test_accuracy_conv(conv_model, valid_x, valid_y)


# In[75]:


def get_model_best_epoch_conv(model_conv):
    model_conv.load_weights('checkpoints_best_only/checkpoint')
    return model_conv


# In[76]:


model_best_epoch_cnn = get_model_best_epoch_conv(get_conv_model(0.25,0.01))
print('Model with best epoch weights:',model_best_epoch_cnn)
get_test_accuracy_conv(model_best_epoch_cnn, X_test, y_test)


# In[77]:


try:
    plt.plot(history_conv.history['accuracy'])
    plt.plot(history_conv.history['val_accuracy'])
except KeyError:
    plt.plot(history_conv.history['acc'])
    plt.plot(history_conv.history['val_acc'])
plt.title('Accuracy vs. epochs')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Training', 'Validation'], loc='lower right')
plt.show()


# In[78]:


plt.plot(history_conv.history['loss'])
plt.plot(history_conv.history['val_loss'])
plt.title('Loss vs. epochs')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Training', 'Validation'], loc='upper right')
plt.show()


# In[82]:


model_json = conv_model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)
model_dir = "Desktop/Flask/venv"
conv_model.save_weights("Desktop/Flask/venv/./model.h5")

