# Part 1 
## Ques  
Write a regex to extract all the numbers with orange color background from the below text in italics.


{"orders":[{"id":1},{"id":2},{"id":3},{"id":4},{"id":5},{"id":6},{"id":7},{"id":8},{"id":9},{"id":10},{"id":11},{"id":648},{"id":649},{"id":650},{"id":651},{"id":652},{"id":653}],"errors":[{"code":3,"message":"[PHP Warning #2] count(): Parameter must be an array or an object that implements Countable (153)"}]}


## Ans:  
The answer to this question is saved in Regex.py file. I have used regex method to solve this question

# Part 2
## Ques
Train a machine learning model (preferably with a neural network) that 
predicts the customer who is going to be checked in. Once done, please test 
the prediction with below test data.
## Ans 
The model is trained using Convolution Neural network. The accuracy of the model is 85%. the test accuracy is 72%. The model is saved in model.h5 to deploy it in later assessment
The model.py will provide you the code.

## Ques
Do a thorough analysis on the results and the dataset with visualizations (please feel free to add creative ways of visualization here).
## Ans
The visulaziation is also stored in model.py file.

##Ques 
Host/Deploy the results using any hosting service you want (streamlit/flask)

## Ans
I have deployed the model using Flask. The model is saved in model.h5 and the template of the web deployment is stored in index.html
The app.py is used to deploy the model in anaconda prompt.


# More Bonus points
## Ques
Write about any difficult problem that you solved.

## Ans
### Problem
The most difficult problem I remember I solved was, when our application crashed. The app that we were relentlessely working on was crashed night before  owing to largest dataset a night before finals. The app was for sentiment analysis using Twitter APIs. The three of us were quite scared due to this. We couldnot find a solution because our finals were dependent on this.
### Solution
A friend of us was also working machine learning app. The good thing was we didnot have to create a new model we used his model and trained on our dataset. The pre-finals we showcased the demo version of this model we used the APIs and structure from there and collectively solved the problem. Our app was not as good as before but it did the work. The first thing we did after app ran was to save the video of the working app in the cd and burn the cd.
### Outcome
After this I always save a backup of all the models.


## Ques
Explain back propagation and tell us how you handle a dataset if 4 out of 30 parameters have null values more than 40 percentage

## Ans

### Back propogation

Back propgation is wdely used in the field of data science it is mainly used in Neural Networks.
It is short form of "back ward propogation of errors".It is method of fine-tunning the weights of a neural network based on error rate obatained in previous epoch. It allows to reduce eerors and make mode more reliable.
It uses gradient descent approach. The main feature of back propogation are the iterative, recursive and efficient methods.

Example: In the above dataset. previously model accuracy was 0.79 in epoch 1 but the accuracy of model was increasing with each epoch and it stopped training at epoch 7 with 0.85 accuracy


### Handling missing data

Before even handling the missing value, I like to visualize the missing values, it can be done using
import missingno as msno

Various methods to analyze missing values are:
1. Dropping the null values, but since the missing values are 40% we can eliminate this option
2. By Statistic
    a. mean
    b. median
3. Imputer
    a. Simple Imputer
    b. Iterative Imputer

I have before analyzed the dataset with similar problem in kaggle.
The link to the file:
https://www.kaggle.com/code/akritisood175/tps-missing-values
