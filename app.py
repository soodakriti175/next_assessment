#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
from flask import Flask, request, render_template, redirect, url_for, request,jsonify
import numpy as np
from tensorflow import keras
from keras.models import load_model
import os


# In[3]:


app = Flask(__name__)
def ValuePredictor(to_predict_list):
    to_predict = np.array(to_predict_list).reshape(1, 12)
    loaded_model = load_model("model1.h5")
    result = loaded_model.predict(to_predict)
    return result[0]

@app.route('/result', methods = ['POST'])
def result():
    if request.method == 'POST':
        to_predict_list = request.form.to_dict()
        to_predict_list = list(to_predict_list.values())
        to_predict_list = list(map(int, to_predict_list))
        result = ValuePredictor(to_predict_list)       
        if int(result)== 1:
            prediction ='CheckedIn'
        else:
            prediction ='NotCheckedIn'           
        return render_template("result.html", prediction = prediction)

